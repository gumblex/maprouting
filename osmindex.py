#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import math
import sqlite3
from xml import sax

import geohash

# Earth mean radius
R_EARTH = 6371000

# distance between two points on a sphere
haversine = lambda lat1, lng1, lat2, lng2: 2 * R_EARTH * math.asin(
    math.sqrt(math.sin(math.radians(lat2 - lat1) / 2) ** 2 +
    math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
    math.sin(math.radians(lng2 - lng1) / 2) ** 2))


class OSMHandler(sax.handler.ContentHandler):
    '''SAX handler for OpenStreetMap XML'''

    def __init__(self, db):
        # db connection
        self.db = db
        self.conn = db.cursor()
        self.context = None
        # known nodes
        self.nodes = {}
        self.way_id = None
        self.way_save = False
        self.way_name = None
        self.way_oneway = False
        self.way_nodes = []

    def startElement(self, name, attrs):
        '''When a element starts'''
        if name == 'node':
            # store nodes in self.nodes, not all get stored in db
            self.context = 'node' if attrs.get(
                'visible', 'true') == 'true' else 'skip'
            self.nodes[int(attrs['id'])] = (
                float(attrs['lat']), float(attrs['lon']))
        elif name == 'way':
            # we have entered a way element, store the status
            self.context = 'way' if attrs.get(
                'visible', 'true') == 'true' else 'skip'
            self.way_id = int(attrs['id'])
            self.way_save = False
            self.way_name = None
            self.way_oneway = False
            self.way_nodes = []
        elif name == 'nd' and self.context == 'way':
            # the nodes on the way
            self.way_nodes.append(int(attrs['ref']))
        elif name == 'tag' and self.context == 'way':
            if attrs['k'] == 'highway':
                if attrs['v'] not in ('path', 'footway', 'bridleway', 'steps', 'cycleway'):
                    self.way_save = True
            elif attrs['k'] == 'name':
                self.way_name = attrs['v']
            elif attrs['k'] == 'oneway':
                self.way_oneway = (attrs['v'] == 'yes')

    def endElement(self, name):
        if name == 'way':
            self.context = None
            if self.way_save and len(self.way_nodes) > 1:
                self.conn.execute('REPLACE INTO ways VALUES (?, ?)',
                                  (self.way_id, self.way_name or None))
                # don't store isolated nodes
                for i in self.way_nodes:
                    # use 62 bit of geohash because SQLite can't store uint64
                    self.conn.execute('REPLACE INTO nodes VALUES (?, ?, ?, ?)',
                        (i, self.nodes[i][0], self.nodes[i][1], geohash.encode_uint64(*self.nodes[i]) >> 2))
                for a, b in zip(self.way_nodes, self.way_nodes[1:]):
                    distance = haversine(self.nodes[a][0], self.nodes[a][1],
                                         self.nodes[b][0], self.nodes[b][1])
                    self.conn.execute('REPLACE INTO edges VALUES (?, ?, ?, ?)',
                                      (a, b, self.way_id, distance))
                    self.conn.execute('REPLACE INTO edges VALUES (?, ?, ?, ?)',
                        (b, a, self.way_id, float('inf') if self.way_oneway else distance))

    def endDocument(self):
        self.db.commit()


def create_db(osmfile='map.osm', dbfile='map.db'):
    db = sqlite3.connect(dbfile)
    conn = db.cursor()

    conn.execute('CREATE TABLE IF NOT EXISTS nodes ('
                 'id INTEGER PRIMARY KEY,'
                 'lat REAL,'
                 'lon REAL,'
                 'geohash INTEGER'
                 ')')
    conn.execute('CREATE TABLE IF NOT EXISTS ways ('
                 'id INTEGER PRIMARY KEY,'
                 'name TEXT'
                 ')')
    conn.execute('CREATE TABLE IF NOT EXISTS edges ('
                 'na INTEGER,'
                 'nb INTEGER,'
                 'way INTEGER,'
                 'distance REAL,'
                 'PRIMARY KEY(na, nb, way),'
                 'FOREIGN KEY(na) REFERENCES nodes(id),'
                 'FOREIGN KEY(nb) REFERENCES nodes(id),'
                 'FOREIGN KEY(way) REFERENCES ways(id)'
                 ')')
    conn.execute('CREATE INDEX IF NOT EXISTS geohash_index ON nodes (geohash)')
    conn.execute('CREATE INDEX IF NOT EXISTS edgena_index ON edges (na)')
    db.commit()
    conn.close()
    handler = OSMHandler(db)
    parser = sax.make_parser()
    parser.setContentHandler(handler)
    parser.parse(osmfile)

if __name__ == '__main__':
    create_db(*sys.argv[1:])
