#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import heapq
import functools
import collections

import geohash

INF = float('inf')
R_EARTH = 6371000
haversine = lambda lat1, lng1, lat2, lng2: 2 * R_EARTH * math.asin(
    math.sqrt(math.sin(math.radians(lat2 - lat1) / 2) ** 2 +
    math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
    math.sin(math.radians(lng2 - lng1) / 2) ** 2))

# bits range [4, 52]
_geohash_radius = (10018863, 5009431, 2504715.5, 1252357.75, 626178.875,
    313089.4375, 156544.7188, 78272.35938, 39136.1797, 19568.0898, 9784.0449,
    4892.0224, 2446.0112, 1223.0056, 611.5028, 305.751, 152.8757, 76.4378,
    38.2189, 19.1095, 9.5547, 4.7774, 2.3889, 1.1943, 0.5971)

@functools.lru_cache(2)
def _precision_convert(distance, err=2):
    for k, r in enumerate(_geohash_radius):
        if r < distance:
            return k * 2 + 2 - err
    return 52

class SimpleGraph:
    '''Simple graph in two-level dicts.'''

    def __init__(self):
        self.d = collections.defaultdict(dict)

    def add_edge(self, src, dst, weight):
        self.d[src][dst] = weight
        self.d[dst][src] = self.d[dst].get(src, INF)

    def length(self, v1, v2):
        if v1 == v2:
            return 0
        else:
            return self.d[v1].get(v2, INF)

    def edges(self):
        for u in self.d:
            for v, weight in self.d[u].items():
                yield u, v, weight

    def nodes(self):
        yield from self.d

    def neighbors(self, node):
        yield from self.d[node].items()

    def __len__(self):
        return len(self.d)

    def __getattr__(self, name):
        return getattr(self.d, name)


class SQLiteGraph:
    '''Immutable graph stored in a SQLite database.'''

    def __init__(self, db):
        self.db = db
        self.count = None

    def length(self, v1, v2):
        if v1 == v2:
            return 0
        conn = self.db.cursor()
        return conn.execute('SELECT distance FROM edges WHERE na = ? AND nb = ?', (v1, v2)).fetchone()[0]

    def edges(self):
        conn = self.db.cursor()
        yield from conn.execute('SELECT na, nb, distance FROM edges')

    def nodes(self):
        conn = self.db.cursor()
        for row in conn.execute('SELECT id FROM nodes'):
            yield row[0]

    def neighbors(self, node):
        conn = self.db.cursor()
        yield from conn.execute('SELECT nb, distance FROM edges WHERE na = ?', (node,))

    def nearest_node(self, lat, lon, distance=5000):
        conn = self.db.cursor()
        gh = geohash.encode_uint64(lat, lon)
        bbmin, bbmax = zip(*geohash.expand_uint64(gh, _precision_convert(distance)))
        try:
            return min(filter(lambda i: i[0] <= distance,
                ((haversine(x[1], x[2], lat, lon), x) for x in conn.execute(
                'SELECT id AS node, lat, lon FROM nodes WHERE geohash BETWEEN ? AND ? ORDER BY abs(geohash - ?) LIMIT 500',
                (min(gh, *bbmin) >> 2, max(gh, *bbmax) >> 2, gh >> 2)))))[1][0]
        except ValueError: # min() arg is an empty sequence
            return

    def nodes_to_way(self, na, nb):
        conn = self.db.cursor()
        res = conn.execute('SELECT ways.id, ways.name, edges.distance FROM ways INNER JOIN edges ON edges.way=ways.id WHERE edges.na = ? AND edges.nb = ?', (na, nb)).fetchone()
        return res

    def node(self, key):
        conn = self.db.cursor()
        return conn.execute('SELECT lat, lon FROM nodes WHERE id = ?', (key,)).fetchone()

    def __len__(self):
        if self.count is None:
            conn = self.db.cursor()
            self.count = conn.execute('SELECT count(*) FROM nodes').fetchone()[0]
        return self.count


def shortest_path(prev, target):
    path = []
    u = target
    while u in prev:
        path.append(u)
        u = prev[u]
    path.append(u)
    path.reverse()
    return path


def dijkstra(graph, source, target):
    '''Dijkstra implementation with a priority queue.'''
    dist = {source: 0}
    prev = {}
    nodes = [(0, source)]
    while nodes:
        weight, u = heapq.heappop(nodes)
        if u not in dist or u == target:
            break
        for v, weight in graph.neighbors(u):
            alt = dist.get(u, INF) + weight
            if alt < dist.get(v, INF):
                dist[v] = alt
                prev[v] = u
                heapq.heappush(nodes, (alt, v))
    return shortest_path(prev, target), dist.get(target, 0)


def bellman_ford(graph, source, target):
    '''Bellman-Ford implementation.'''
    dist = {}
    dist[source] = 0
    prev = {}
    for i in range(len(graph) - 1):
        for u, v, weight in graph.edges():
            if dist.get(u, INF) + weight < dist.get(v, INF):
                dist[v] = dist[u] + weight
                prev[v] = u
    # check for negative-weight cycles
    #for u, v, weight in graph.edges():
        #assert dist.get(v, INF) <= dist.get(u, INF) + weight
    return shortest_path(prev, target), dist.get(target, 0)


def floyd_warshall(graph, source, target):
    '''Floyd-warshall implementation.'''
    dist = {}
    prec = {}
    nodes = []
    for u, v, w in graph.edges():
        dist[(u, u)] = 0
        dist[(u, v)] = w
        if w != INF:
            prec[(u, v)] = v
        nodes.append(u)
    nodes = frozenset(nodes)
    for k in nodes:
        for i in nodes:
            for j in nodes:
                alt = dist.get((i, k), INF) + dist.get((k, j), INF)
                if dist.get((i, j), INF) > alt:
                    dist[(i, j)] = alt
                    prec[(i, j)] = prec[(i, k)]
    del nodes
    if (source, target) not in prec:
        return [], 0
    u = source
    path = [u]
    while u != target:
        u = prec[(u, target)]
        path.append(u)
    return path, dist.get((source, target), 0)


def route(graph, source, target, func=dijkstra):
    return func(graph, source, target)

if __name__ == '__main__':
    graph = SimpleGraph()
    graph.add_edge('A', 'B', 10)
    graph.add_edge('A', 'C', 20)
    graph.add_edge('B', 'D', 15)
    graph.add_edge('C', 'D', 30)
    graph.add_edge('B', 'E', 50)
    graph.add_edge('D', 'E', 30)
    graph.add_edge('E', 'F', 5)
    graph.add_edge('F', 'G', 2)
    print(route(graph, 'A', 'D', dijkstra))
    print(route(graph, 'A', 'D', bellman_ford))
    print(route(graph, 'A', 'D', floyd_warshall))
