#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import sqlite3
import functools

import bottle
import routing

application = bottle.default_app()

DB = sqlite3.connect('map.db')
GRAPH = routing.SQLiteGraph(DB)
MAX_DIST = 5000


def direction(lata, lona, latb, lonb):
    n = 0
    lata, lona, latb, lonb = tuple(map(math.radians, (lata, lona, latb, lonb)))
    if lata != latb:
        n = math.acos(math.asin(math.sqrt(pow((math.sin(latb) - math.sin(lata)), 2) + pow((math.cos(latb) - math.cos(lata)), 2)) / 2) / math.asin(math.sqrt(pow((math.sin(latb) - math.sin(lata)), 2) + pow((math.cos(latb) - math.cos(lata) * math.cos(abs(lona - lonb))), 2) + pow((math.cos(lata) * math.sin(abs(lona - lonb))), 2)) / 2))
    if lata < latb:
        n = math.pi - n
    if lona < lonb:
        n = -n
    return n


nearest_node = functools.lru_cache(4)(GRAPH.nearest_node)

@functools.lru_cache(16)
def route(src, dst):
    path, full_dist = routing.route(GRAPH, src, dst)
    nodes = list(map(GRAPH.node, path))
    directions = []
    segment = []
    segdist = 0
    lastname, lastdirct, dirct = '', None, None
    for key, ends in enumerate(zip(path, path[1:])):
        way, name, dist = GRAPH.nodes_to_way(*ends)
        a, b = nodes[key], nodes[key + 1]
        dirct = direction(a[0], a[1], b[0], b[1])
        if lastdirct is None:
            segment.append(a)
            lastdirct = dirct
        alpha = dirct - lastdirct
        segment.append(b)
        segdist += dist
        lastdirct = dirct
        if name != lastname or alpha > math.pi / 4:
            directions.append({
                'direction': round(alpha * 4 / math.pi) % 8,
                'name': name, 'distance': segdist, 'segment': segment
            })
            segment = [b]
            segdist = 0
            lastname = name
    if segdist:
        directions.append({
            'direction': round(alpha * 4 / math.pi) % 8,
            'name': name, 'distance': segdist, 'segment': segment
        })
    return {
        'distance': full_dist,
        'directions': directions,
        'nodes': nodes,
    }


@bottle.route('/route/')
def route_api():
    try:
        lata = float(bottle.request.query['lata'])
        lona = float(bottle.request.query['lona'])
        latb = float(bottle.request.query['latb'])
        lonb = float(bottle.request.query['lonb'])
    except Exception:
        bottle.response.status = 400
        return {'error': 'invalid parameters'}
    try:
        src = nearest_node(lata, lona, MAX_DIST)
        dst = nearest_node(latb, lonb, MAX_DIST)
        if src is None or dst is None:
            bottle.response.status = 404
            return {'error': 'not enough data'}
        ret = route(src, dst)
        if not ret['distance']:
            bottle.response.status = 404
        return ret
    except Exception as ex:
        bottle.response.status = 500
        return {'error': repr(ex)}


@bottle.route('/')
def index():
    return bottle.static_file('frontend.html', root='.')

if __name__ == '__main__':
    bottle.run(host='0.0.0.0', port=6700, server='auto')
